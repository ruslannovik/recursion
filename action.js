const data = {
    value: 10,
    state: {
        value: 2,
        state: {
            value: 12,
            state: {
                value: 1,
                state: {
                    value: 4,
                    state: {
                        value: 11,
                        state: null
                    }
                }
            }
        }
    }
}

/*
it's not true
const summ = data.value + data.state.value + ...
*/

function getSumm(obj) {
    if (obj.state) {
        return obj.value + getSumm(obj.state);
    }
    return obj.value;
}

const summ = getSumm(data);
console.log(summ)

// ---------------------------------

const videoTag = document.querySelector('#video');
const playBtn = document.querySelector('#play');
const pauseBtn = document.querySelector('#pause');
 
const durationSpan = document.querySelector('.duration');
const currentSpan = document.querySelector('.currentTime');

let currentTimeInterval;

playBtn.addEventListener('click', playVideo);

pauseBtn.addEventListener('click', pauseVideo);

// setTimeout(() => videoTag.play(), 2000)
// setTimeout(() => videoTag.pause(), 4000)

function getDuration() {
    const duration = videoTag.duration;
    const minutes = Math.round(duration / 60);
    const seconds = Math.round(duration - minutes * 60);
    durationSpan.innerText = `${minutes > 9 ? minutes : '0' + minutes}:${seconds > 9 ? seconds : '0' + seconds}`;
}

function playVideo() {
    videoTag.play();
    if(!currentTimeInterval)
    currentTimeInterval = setInterval(() => {
        currentSpan.innerText = videoTag.currentTime;
        console.log(1);
    }, 1000);
}

function pauseVideo() {
    videoTag.pause();
    clearInterval(currentTimeInterval);
}

videoTag.onloadedmetadata = getDuration;

 
// setInterval(() => {
//     console.log(videoTag.currentTime);
//     console.log(videoTag.duration);
// })